#!/usr/bin/env bash
if [ -n "$DEBUG_RTDEV" ] || [ -n "$GITPOD_HEADLESS" ]; then
    set -x
fi

# Measure the time it takes to go through the script
script_start_time=$(date +%s)

if [ ! -f "${GITPOD_REPO_ROOT}/.env" ]; then
   echo $(echo $)
fi