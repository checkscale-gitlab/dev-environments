# Recap Time Squad's Dev Environment

This is where we build `quay.io/gitpodified-workspace-images/recaptime-dev-environment`, packed with Terraform, Railway CLI and even some k8s tools. It'll use Zsh with Oh My Zsh preinstalled as the default shell, but you can `exec bash` away.

## Get Started

There are two ways to get things started using our dev environment Docker image. Remember that we only support amd64-based Linux machines, so consider building from source if needed.

### Using our devenv link generator

We recommend using our devenv link generator for that, as we handle the generation of `gitpod.io/#` links ~~on Darklang servers~~ our own Expressjs server on Railway.

```bash
# quick shortcut with defaults
https://devenv.rtapp.tk/new

# cursed secrets in form of key-value pairs separated by spaces
# used for some gp env -e magic behind the scenes.
# Converts to the following URL from our backend:
# https://gitpod.io/#RTDEV_BASE64_SECRET=R0lUUE9EPTEgQkFTRTY0PWlsZnZqZGV2Yml2aGxsdmxpZmhpbAo=/#https://gitlab.com/RecapTime/infra/dev-environments
# REMEMBER DO NOT LEAK THAT BASEE64-ENCODED VALUE!
https://devenv.rtapp.tk/new?secret=R0lUUE9EPTEgQkFTRTY0PWlsZnZqZGV2Yml2aGxsdmxpZmhpbAo=

# Clone an starter repo to bootstrap with custom dirname within $GITPOD_REPO_ROOT/repos
# Converts to the following URL from our backend:
# https://gitpod.io/#RTDEV_REPO_URL=https://gitlab.com/gitpodify/gitpodified-workspace-images,RTDEV_REPO_CLONE_DIR=gp-ws-images/https://gitlab.com/RecapTime/infra/dev-environments
https://devenv.rtapp.tk/new?repoUrl=https://gitlab.com/gitpodify/gitpodified-workspace-images&repoCloneDir=gp-ws-images
```

The table below lists all the supported paramters for the `devenv.rtapp.tk/new` endpoint. If the `devenv.rtapp.tk` experienced some `NXDOMAIN` issues or HSTS errors, try using `recaptime-devenv.up.railway.app` as the hostname. We also provide the ENV counterparts as needed, in case you're gonna generate your own link by hand.s

| URL Parameter Name | Description | Example Value | ENV for `gitpod.io/#` link |
| --- | --- | --- | --- |
| `secret` | Base64-encoded string in form of `key=value` pairs separated by spaces. | `R0lUUE9EPTEgQkFTRTY0PWlsZnZqZGV2Yml2aGxsdmxpZmhpbAo=` | `RTDEV_BASE64_SECRET`
| `repoUrl` | Git repository URL for our bootstrap scripts to run. It'll look for `scripts/bootstrap` or `scripts/setup` script files. Since workspaces are ephermal by design, we recommend [using this template][devenv-template] instead. | `https://gitlab.com/gitpodify/gitpodified-workspace-images` | `RTDEV_REPO_URL`
| `repoCloneDir` | Target directory where to clone the repository, requires `repoUrl` to be used | `gp-ws-images` | `RTDEV_REPO_CLONE_DIR` |
| `runInsideContainer` | Set it to `true` or `1` to create an devenv container with OpenVSCode Server inside an workspace, currently WIP. | `false` | `RTDEV_GP_DEVCONTAINER` |

[devenv-template]: https://gitlab.com/gitpodify/templates/recaptime-devenv-repo

Your secrets will be kept in `.env` on `$GITPOD_REPO_ROOT`, so you can simply use the default link and copy your `.env` between workspaces. You'll just have one `reload-env` away.

### Use as your Gitpod workspace image

Depending on how you configure your workspace image, it can be either:

* using the `image` key in Gitpod config:

```yaml
image: quay.io/gitpodified-workspace-images/recaptime-dev-environment
```

* setting `quay.io/gitpodified-workspace-images/recaptime-dev-environment` as base image in your `.gitpod.Dockerfile`

```dockerfile
FROM quay.io/gitpodified-workspace-images/recaptime-dev-environment

# rest of your Dockerfile goes here
```

## Documentation

It's still an work in progress right now, so heare are some:

### Generating your `secret` parameter

```bash
# This script will do some magic behind the scenes to generate your bas64 string for you.
# Source: https://gitlab.com/RecapTime/infra/dev-environments/blob/main/scripts/generate-base64-string
generate-base64-string examples/your-cursed-keys-go-here.env
```
